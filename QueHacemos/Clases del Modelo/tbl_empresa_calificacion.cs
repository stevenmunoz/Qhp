//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace jardinMvc.Context
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_empresa_calificacion
    {
        public int empCalificacion_id { get; set; }
        public int empresa_id { get; set; }
        public int tipCalifica_id { get; set; }
    
        public virtual tbl_empresa tbl_empresa { get; set; }
        public virtual tbl_tipo_calificacion tbl_tipo_calificacion { get; set; }
    }
}
