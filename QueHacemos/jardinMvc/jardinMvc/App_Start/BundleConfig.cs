﻿using System.Web;
using System.Web.Optimization;

namespace jardinMvc
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/esperanos").Include(
                        "~/Scripts/jquery.countdown.js",
                        //"~/Scripts/index.js",
                        "~/Scripts/index_categorias.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/js").Include(
                 "~/Scripts/bootstrap.min.js"
                ));

            bundles.Add(new StyleBundle("~/content/css").Include(
                "~/Content/bootstrap.min.css"));


            bundles.Add(new StyleBundle("~/content/categorias").Include(
                "~/Content/categories.css",
                "~/Content/font.css",
                "~/Content/index.css",
                "~/Content/global.css"));

            bundles.Add(new StyleBundle("~/content/css_error").Include(
               "~/Content/error.css",
               "~/Content/font.css"));

            bundles.Add(new StyleBundle("~/content/esperanos").Include(
                "~/Content/font.css",
                "~/Content/index.css"));

            bundles.Add(new StyleBundle("~/content/css_contacto").Include(
               "~/Content/bootstrap1.css",
               "~/assets/css/bootstrap-modal-bs3patch.css",
               "~/assets/css/bootstrap-modal.css"));

            bundles.Add(new StyleBundle("~/content/css_empresas").Include(
               "~/Content/font.css",
               "~/Content/index1.css",
               "~/Content/categories1.css",
               "~/Content/category1.css",
               "~/Content/global.css"));

            bundles.Add(new StyleBundle("~/content/css_perfiles").Include(
               "~/Content/font.css",
               "~/Content/index.css",
               "~/Content/categories1.css",
               "~/Content/profile.css",
               "~/Content/global.css",
               "~/Content/popover.css"));

            bundles.Add(new StyleBundle("~/content/css_admin_empresas").Include(
               "~/Content/font.css",
               "~/Content/index.css",
               "~/Content/categories1.css",
               "~/Content/form.css"));

            bundles.Add(new StyleBundle("~/css_rating").Include(
            "~/Content/allinone_carousel.css",
            "~/Content/jRating.jquery.css"));


            bundles.Add(new ScriptBundle("~/content/js_contacto").Include(
               "~/Scripts/jquery-1.9.1.js",
                "~/assets/js/bootstrap-modalmanager.js",
               "~/assets/js/bootstrap-modal.js",
               "~/assets/js/jquery.validate.js",
               "~/Scripts/index_categorias.js"));

            bundles.Add(new ScriptBundle("~/content/js_contactoPerfil").Include(
               "~/Scripts/jquery-1.8.2.js",
                "~/assets/js/bootstrap-modalmanager.js",
               "~/assets/js/bootstrap-modal.js",
               "~/Scripts/empresa_contacto.js",
               "~/assets/js/jquery.validate.js",
               "~/Scripts/tooltip.js"));

            bundles.Add(new ScriptBundle("~/content/js_perfil").Include(
               "~/Scripts/jRating.jquery.js",
               "~/Scripts/js/jquery-ui-1.8.16.custom.min.js",
               "~/Scripts/js/jquery.ui.touch-punch.min.js",
               "~/Scripts/js/allinone_carousel.js",
               "~/assets/js/jquery.validate.js"));
        }
    }
}