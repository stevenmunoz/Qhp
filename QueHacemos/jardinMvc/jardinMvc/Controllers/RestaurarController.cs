﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using jardinMvc.Context;
using jardinMvc.Models;
using System.Data;
using System.Net.Mail;

namespace jardinMvc.Controllers
{
    public class RestaurarController : Controller
    {
        private UnifyEntities db = new UnifyEntities();
        //
        // GET: /Restaurar/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        //[RecaptchaControlMvc.CaptchaValidatorAttribute]
        public ActionResult Restablecer(string inpCorreo)
        {
            if (IsValid(inpCorreo))
            {
                string usuario = getUsuarioPorCorreo(inpCorreo);

                if (String.IsNullOrWhiteSpace(usuario))
                {
                    TempData["Message"] = "inexistente";
                }
                else
                {
                    //Generamos un token para enviar al usuario por correo
                    var token = WebSecurity.GeneratePasswordResetToken(usuario);

                    tbl_empresa emp = db.tbl_empresa.Where(m => m.userName == usuario).FirstOrDefault();
                    emp.empresa_token = token;
                    db.Entry(emp).State = EntityState.Modified;
                    db.SaveChanges();

                    usuario = AlsusEncrypt.Alsus.Encrypt(usuario, "X81&sP)Wk5j0(Rm=q9Z4s$7Y", "gsC3)h#8kmLy%Z", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256);

                    //Generamos el link para enviar al usuario
                    var link = "<a href='" + Url.Action("Recuperar", "Restaurar", new { un = usuario, rt = token }, "http") + "'>Restablecer Contraseña</a>";

                    string subject = "Restauración de contraseña Que hacemos pues";
                    string body = "<b>Para restaurar su contraseña de usuario de Que hacemos pues haga click en el siguiente enlace</b><br/>" + link; //edit it
                    try
                    {
                        if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", inpCorreo, subject, body.ToString(), "gerencia@quehacemospues.com", "Que Hacemos Pues", "0"))
                        {
                            TempData["Message"] = "email_enviado";
                        }
                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "email_invalido";
                    }
                }
            }
            else {
                TempData["Message"] = "email_invalido";
            }
            
            return RedirectToAction("Login", "Account");
        }

        public string getUsuarioPorCorreo(string correo)
        {

            correo = correo.Trim();
            string nombreUsu = db.tbl_empresa.Where(m => m.empresa_correo == correo).Select(m => m.userName).FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(nombreUsu))
            {
                return nombreUsu;
            }
            else
            {
                return "";
            }
        }

        [AllowAnonymous]
        public ActionResult Recuperar(string un, string rt)
        {
            un = AlsusEncrypt.Alsus.Decrypt(un, "X81&sP)Wk5j0(Rm=q9Z4s$7Y", "gsC3)h#8kmLy%Z", "SHA1", 2, "@1B2c3D4e5F6g7H8", 256);

            //verificamos que userId y el token de verificación coincidan
            bool match = (from j in db.tbl_empresa
                          where (j.userName == un)
                           && (j.empresa_token == rt)
                          //&& (j.PasswordVerificationTokenExpirationDate < DateTime.Now)
                          select j).Any();
            //Si coinciden
            if (match)
            {
                //generar un password aleatorio
                string newpassword = GenerateRandomPassword(8);
                //reseteamos el password
                bool response = WebSecurity.ResetPassword(rt, newpassword);
                //Enviamos el email con el nuevo password
                string email = (from tblDatos in db.tbl_empresa
                                where tblDatos.userName == un
                                select tblDatos.empresa_correo).FirstOrDefault();

                string subject = "Contraseña Restaurada Que hacemos pues";
                string body = "<b>Estimado/a Usuario </b>, <br/><br/> Este correo electrónico contiene una contraseña temporal. Note que su contraseña temporal contiene mayúsculas y minúsculas, ingréselo exactamente como se muestra a continuación <br /><br /> <b> Contraseña Temporal: </b>" + newpassword + "<br/><br/> Inicie sesión en el vínculo mostrado abajo usando la contraseña temporal. Una vez iniciada sesión actualice sus datos.<br/><br/> http://quehacemospues.com/Account/Login"; //edit it
                try
                {
                    if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", email, subject, body.ToString(), "gerencia@quehacemospues.com", "Que Hacemos Pues", "0"))
                    {
                        TempData["Message"] = "contrasena_cambiada";
                    }
                }
                catch (Exception ex)
                {
                    TempData["Message"] = "email_rest_invalido";
                }
            }

            return RedirectToAction("Login", "Account");

        }

        private string GenerateRandomPassword(int length)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-*&#+";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        public bool IsValid(string emailaddress)
        {
            if (String.IsNullOrWhiteSpace(emailaddress)) {
                return false;
            }
            else  {
                try
                {
                    MailAddress m = new MailAddress(emailaddress);
                    return true;
                }
                catch (FormatException)
                {
                    return false;
                }
            }
        }

    }
}
