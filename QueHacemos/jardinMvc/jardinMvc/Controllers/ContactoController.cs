﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using jardinMvc.Models;

namespace jardinMvc.Controllers
{
    public class ContactoController : Controller
    {

        private UnifyEntities db = new UnifyEntities();
        //
        // GET: /Contacto/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EnviarMensaje(string name, string email, string message, string tipo, int? categoria)
        {
            //Cremos el código html del mensaje que será enviado al correo
            string mensaje = "<div style='width:647px; font: 11pt morgansanscond_officeregular,Tahoma,Arial,Verdana,Calibri,Arial,Sans-Serif;'><br />";
            mensaje = mensaje + "<strong>Que Hacemos Pues Manizales</strong><br/><br/>Un usuario quiere contactarse contigo desde el sitio web. <br/> a continuación se  presentan los detalles: <br/><br/><br/> <strong>Nombre : </strong>  " + name + " <br/>  <strong>Email : </strong>  " + email + " <br/> <strong>Mensaje : </strong>  " + message + " <br/> <br/>";
            mensaje = mensaje + "<br/><br/><strong>Que Hacemos Pues Manizales</strong><br /><br> <strong> Este es un correo electrónico automático, por favor no responda este mensaje</strong>.</div>";

            //if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", "mercadeoyventas@quehacemospues.com", "Contacto", mensaje, "gerencia@quehacemospues.com", "Que Hacemos Pues", "0"))
            if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", "mercadeoyventas@quehacemospues.com", "Contacto", mensaje, "gerencia@quehacemospues.com", "Que Hacemos Pues", "0"))
            {
                TempData["correo"] = 1;
                
                if (tipo.Equals("esperanos")) 
                {
                    return RedirectToAction("Esperanos", "Home");
                }
                else if (tipo.Equals("inicio")) 
                {
                    return RedirectToAction("Index", "Home");
                }
                else if(tipo.Equals("categoria"))
                {
                    tbl_categoria tbl_categoria = db.tbl_categoria.Find(categoria);
                    return RedirectToAction("QueHacemosPues", "Categoria", new { id = categoria, nombre = tbl_categoria.categoria_nombre });
                }
                
            }
            else
            {
                TempData["correo"] = 0;

                if (tipo.Equals("esperanos"))
                {
                    return RedirectToAction("Esperanos", "Home");
                }
                else if (tipo.Equals("inicio"))
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (tipo.Equals("categoria"))
                {
                    tbl_categoria tbl_categoria = db.tbl_categoria.Find(categoria);
                    return RedirectToAction("QueHacemosPues", "Categoria", new { id = categoria, nombre = tbl_categoria.categoria_nombre });
                }
            }

            return null;

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContacoEmpresa(string nomContact, string corContact, string mensContact, int empresa_id)
        {
            //Cremos el código html del mensaje que será enviado al correo
            string mensaje = "<div style='width:647px; font: 11pt morgansanscond_officeregular,Tahoma,Arial,Verdana,Calibri,Arial,Sans-Serif;'><br />";
            mensaje = mensaje + "<strong>Que Hacemos Pues Manizales</strong><br/><br/>Un usuario quiere contactarse contigo desde el sitio web. <br/> a continuación se  presentan los detalles: <br/><br/><br/> <strong>Nombre : </strong>  " + nomContact + " <br/>  <strong>Email : </strong>  " + corContact + " <br/> <strong>Mensaje : </strong>  " + mensContact + " <br/> <br/>";
            mensaje = mensaje + "<br/><br/><strong>Que Hacemos Pues Manizales</strong><br /><br> <strong> Este es un correo electrónico automático, por favor no responda este mensaje</strong>.</div>";

            tbl_empresa empresa = db.tbl_empresa.Find(empresa_id);

            //if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", "mercadeoyventas@quehacemospues.com", "Contacto", mensaje, "gerencia@quehacemospues.com", "Que Hacemos Pues", "0"))
            if (Correo.SendMail("no-reply@quehacemospues.com", "Noreply0800", empresa.empresa_correo, "Contacto", mensaje, "", "Que Hacemos Pues", "0"))
            {
                TempData["correo"] = 1;
                return RedirectToAction("Empresa", "Perfil", new { id = empresa.empresa_id });
            }
            else
            {
                TempData["correo"] = 0;
                return RedirectToAction("Empresa", "Perfil", new { id = empresa.empresa_id }); 
            }
        }

    }
}
