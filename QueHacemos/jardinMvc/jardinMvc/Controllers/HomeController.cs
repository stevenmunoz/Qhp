﻿using jardinMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using jardinMvc.Models;
using System.Collections;


namespace jardinMvc.Controllers
{
    public class HomeController : Controller
    {
        private UnifyEntities db = new UnifyEntities();
      
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Duration=60)]
        public ActionResult GetListadoEventos()
        {
            //var eventos = db.tbl_evento_empresa.Where(m => m.evento_fechaInicio <= DateTime.Now && DateTime.Now < m.evento_fechaFin && m.tbl_empresa.empresa_estado).OrderByDescending(m => m.evento_fechaInicio);
            var eventos = from evento in db.tbl_evento_empresa.Where(m => m.evento_fechaInicio <= DateTime.Now && DateTime.Now < m.evento_fechaFin && m.tbl_empresa.empresa_estado).OrderByDescending(m => m.evento_fechaInicio)
                          select new
                          {
                              empresaId = evento.tbl_empresa.empresa_id,
                              eventoNombre = evento.evento_nombre,
                              empresaImagen = evento.tbl_empresa.empresa_imagen,
                              empresaNombreUrl = evento.tbl_empresa.empresa_nombre_url,
                              eventoFecha = evento.evento_fechaEvento,
                              eventoDescripcion = evento.evento_descripcion
                          };

            return Json(eventos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Esperanos() 
        {
            return View();
        }

        public ActionResult Inicio() 
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Perfil(int id)
        {
            //tbl_empresa empresa = db.tbl_empresa.Find(id);
            return View();
        }
               
    }
}
