﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jardinMvc.Controllers
{
    public class LogoutController : Controller
    {
        //
        // GET: /Logout/

        public ActionResult Index()
        {
            Session["Nombre"] = null;
            Session["Empresa"] = "";
            return RedirectToAction("Login", "Account");
        }

    }
}
