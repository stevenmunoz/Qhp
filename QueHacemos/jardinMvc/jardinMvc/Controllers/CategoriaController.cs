﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using System.Collections;
using PagedList;
using jardinMvc.Models;

namespace jardinMvc.Controllers
{
    public class CategoriaController : Controller
    {
        //
        // GET: /Categoria/
        private UnifyEntities entities = new UnifyEntities();

        public ActionResult QueHacemosPues(int? id, string nombre, int? pagina, int? subcategoria)
        {
            int numeroPagina = pagina ?? 1;
            int tamanioPagina = 16;
            List<tbl_empresa> lista;
            List<tbl_empresa> listaUrls = new List<tbl_empresa>();
            List<tbl_empresa> empSubcategorias;

            //List<EmpresaCalificacion> empCalificadas;
            //List<EmpresaCalificacion> empCalificadasSubcat;

            TempData["subcatId"] = "";

            if (subcategoria == null || subcategoria == 0)
            {
                lista = entities.tbl_empresa.Where(m => m.categoria_id == id && m.empresa_estado && m.empresa_descripcion != "" && m.empresa_correo != "" && m.empresa_telefono != null).ToList();
                
                //Retorna la lista de empresas con su calificación
                //empCalificadas = empresasCalificadas(lista);

                var paginaEmpresas = lista.ToPagedList(numeroPagina, tamanioPagina);
                TempData["categoriaId"] = id;
                TempData["categoriaNombre"] = nombre;
                return View(paginaEmpresas);
            }
            else
            {
                tbl_subcategoria subcat = entities.tbl_subcategoria.Find(subcategoria);
                empSubcategorias = subcat.tbl_empresa.Where(m => m.empresa_estado && m.empresa_descripcion != "" && m.empresa_correo != "" && m.empresa_telefono != null).ToList();

                //Retorna la lista de empresas pertenecientes a una subcategoría con su calificación
                //empCalificadasSubcat = empresasCalificadas(empSubcategorias);

                var paginaEmpSubcat = empSubcategorias.ToPagedList(numeroPagina, tamanioPagina);
                TempData["categoriaId"] = id;
                TempData["categoriaNombre"] = nombre;
                TempData["subcatId"] = subcategoria;
                return View(paginaEmpSubcat);

            }
        }

       
        public List<tbl_categoria> CategoriasMenu()
        {
            return entities.tbl_categoria.ToList();
        }

        public string CategoriaImagen(int categoria_id)
        {
            if (categoria_id != 0)
            {
                tbl_categoria categoria = entities.tbl_categoria.Where(m => m.categoria_id == categoria_id).First();
                return categoria.categoria_imagen;
            }

            return String.Empty;
        }

        public List<SelectListItem> SelectSubcategorias(int categoria_id)
        {
            List<tbl_subcategoria> subcategorias = entities.tbl_subcategoria.Where(m => m.categoria_id == categoria_id).ToList();
            List<SelectListItem> listSubcategorias = new List<SelectListItem>();

            listSubcategorias.Add(new SelectListItem { Value = "0", Text = "todas", Selected = true });
            foreach (var item in subcategorias)
            {
                listSubcategorias.Add(new SelectListItem { Value = item.subcategoria_id.ToString(), Text = item.subcategoria_nombre, Selected = false });
            }

            return listSubcategorias;

        }

        [HttpPost]
        public ActionResult Buscar(string searchTerm)
        {
            List<tbl_empresa> lista;
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                lista = entities.tbl_empresa.Where(x => x.empresa_nombre.Contains(searchTerm) && x.empresa_estado && x.empresa_descripcion != "" && x.empresa_correo != "" && x.empresa_telefono != null).ToList();
               
                var paginaEmpresas = lista.ToPagedList(1, 16);
                TempData["categoriaId"] = 1;
                TempData["categoriaNombre"] = "Restaurantes";
                return View("QueHacemosPues", paginaEmpresas);
            }
            else 
            {
                lista = entities.tbl_empresa.Where(x => x.empresa_estado && x.empresa_descripcion != "" && x.empresa_correo != "" && x.empresa_telefono != null).ToList();

                var paginaEmpresas = lista.ToPagedList(1, 16);
                TempData["categoriaId"] = 1;
                TempData["categoriaNombre"] = "Restaurantes";
                return View("QueHacemosPues", paginaEmpresas);
            }
        }

        public JsonResult getEmpresasAutocomplete(string term)
        {
            List<string> empresas;

            empresas = entities.tbl_empresa.Where(x => x.empresa_nombre.StartsWith(term) && x.empresa_estado && x.empresa_descripcion != "" && x.empresa_correo != "" && x.empresa_telefono != null).Select(y => y.empresa_nombre).ToList();

            return Json(empresas, JsonRequestBehavior.AllowGet);

        }

        protected override void Dispose(bool disposing)
        {
            entities.Dispose();
            base.Dispose(disposing);
        }

    }
}
