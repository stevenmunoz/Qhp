﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;

namespace jardinMvc.Controllers
{
    public class EventoEmpresaController : Controller
    {
        private UnifyEntities db = new UnifyEntities();
       
        //
        // GET: /EventoEmpresa/

        public ActionResult Index(int id = 0)
        {
            List<tbl_evento_empresa> eventos = db.tbl_evento_empresa.Where(t => t.empresa_id == id).Include(t => t.tbl_empresa).OrderBy(m => m.evento_fechaEvento).ToList();
            List<tbl_evento_empresa> eventosAux = new List<tbl_evento_empresa>();
            foreach (var item in eventos)
            {
                string descripcion = item.evento_descripcion.Length > 40 ? item.evento_descripcion.Substring(0, 40) + "..." : item.evento_descripcion;
                item.evento_descripcion = descripcion;
                eventosAux.Add(item);
            }
            return View(eventosAux);
        }

        //
        // GET: /EventoEmpresa/Details/5

        public ActionResult Details(int id = 0)
        {
            tbl_evento_empresa tbl_evento_empresa = db.tbl_evento_empresa.Find(id);
            if (tbl_evento_empresa == null)
            {
                return HttpNotFound();
            }
            return View(tbl_evento_empresa);
        }

        //
        // GET: /EventoEmpresa/Create

        public ActionResult Create()
        {
            ViewBag.empresa_id = new SelectList(db.tbl_empresa, "empresa_id", "empresa_nombre");
            return View();
        }

        //
        // POST: /EventoEmpresa/Create

        [HttpPost]
        public ActionResult Create(tbl_evento_empresa tbl_evento_empresa)
        {
            if (ModelState.IsValid)
            {
                tbl_evento_empresa.evento_id = 1;
                db.tbl_evento_empresa.Add(tbl_evento_empresa);
                db.SaveChanges();
                return RedirectToAction("Index", "EventoEmpresa", new { id = tbl_evento_empresa.empresa_id });
            }

            ViewBag.empresa_id = new SelectList(db.tbl_empresa, "empresa_id", "empresa_nombre", tbl_evento_empresa.empresa_id);
            return View(tbl_evento_empresa);
        }

        //
        // GET: /EventoEmpresa/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbl_evento_empresa tbl_evento_empresa = db.tbl_evento_empresa.Find(id);
            if (tbl_evento_empresa == null)
            {
                return HttpNotFound();
            }
            ViewBag.empresa_id = new SelectList(db.tbl_empresa, "empresa_id", "empresa_nombre", tbl_evento_empresa.empresa_id);
            return View(tbl_evento_empresa);
        }

        //
        // POST: /EventoEmpresa/Edit/5

        [HttpPost]
        public ActionResult Edit(tbl_evento_empresa tbl_evento_empresa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_evento_empresa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "EventoEmpresa", new { id = tbl_evento_empresa.empresa_id });
            }
            ViewBag.empresa_id = new SelectList(db.tbl_empresa, "empresa_id", "empresa_nombre", tbl_evento_empresa.empresa_id);
            return View(tbl_evento_empresa);
        }

        //
        // GET: /EventoEmpresa/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_evento_empresa tbl_evento_empresa = db.tbl_evento_empresa.Find(id);
            if (tbl_evento_empresa == null)
            {
                return HttpNotFound();
            }
            return View(tbl_evento_empresa);
        }

        //
        // POST: /EventoEmpresa/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_evento_empresa tbl_evento_empresa = db.tbl_evento_empresa.Find(id);
            db.tbl_evento_empresa.Remove(tbl_evento_empresa);
            db.SaveChanges();
            return RedirectToAction("Index", "EventoEmpresa", new { id = tbl_evento_empresa.empresa_id });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}