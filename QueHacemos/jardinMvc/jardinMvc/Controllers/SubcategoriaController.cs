﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using System.Collections;

namespace jardinMvc.Controllers
{
    public class SubcategoriaController : Controller
    {
        private UnifyEntities db = new UnifyEntities();

        //
        // GET: /Subcategoria/

        public ActionResult Index()
        {
            var tbl_subcategoria = db.tbl_subcategoria.Include(t => t.tbl_categoria);
            return View(tbl_subcategoria.ToList());
        }

        //
        // GET: /Subcategoria/Details/5

        public ActionResult Details(int id = 0)
        {
            tbl_subcategoria tbl_subcategoria = db.tbl_subcategoria.Find(id);
            if (tbl_subcategoria == null)
            {
                return HttpNotFound();
            }
            return View(tbl_subcategoria);
        }

        //
        // GET: /Subcategoria/Create

        public ActionResult Create()
        {
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre");
            return View();
        }

        //
        // POST: /Subcategoria/Create

        [HttpPost]
        public ActionResult Create(tbl_subcategoria tbl_subcategoria)
        {
            if (ModelState.IsValid)
            {
                db.tbl_subcategoria.Add(tbl_subcategoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_subcategoria.categoria_id);
            return View(tbl_subcategoria);
        }

        //
        // GET: /Subcategoria/Edit/5

        public ActionResult Edit(int id = 0)
        {
            tbl_subcategoria tbl_subcategoria = db.tbl_subcategoria.Find(id);
            if (tbl_subcategoria == null)
            {
                return HttpNotFound();
            }
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_subcategoria.categoria_id);
            return View(tbl_subcategoria);
        }

        //
        // POST: /Subcategoria/Edit/5

        [HttpPost]
        public ActionResult Edit(tbl_subcategoria tbl_subcategoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tbl_subcategoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_subcategoria.categoria_id);
            return View(tbl_subcategoria);
        }

        //
        // GET: /Subcategoria/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_subcategoria tbl_subcategoria = db.tbl_subcategoria.Find(id);
            if (tbl_subcategoria == null)
            {
                return HttpNotFound();
            }
            return View(tbl_subcategoria);
        }

        //
        // POST: /Subcategoria/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_subcategoria tbl_subcategoria = db.tbl_subcategoria.Find(id);
            db.tbl_subcategoria.Remove(tbl_subcategoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public static IEnumerable SelectCategorias()
        {
            UnifyEntities db = new UnifyEntities();

            var select = from lista in db.tbl_categoria
                         select new { id = lista.categoria_id, nombre = lista.categoria_nombre };
            return select;
        }

        public ActionResult listadoSubcategorias(int categoria_id)
        {
            UnifyEntities entities = new UnifyEntities();

            var resultado = from lista in entities.tbl_subcategoria
                            where lista.categoria_id == categoria_id
                            select new { identificador = lista.subcategoria_id, nombre = lista.subcategoria_nombre };

            return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}