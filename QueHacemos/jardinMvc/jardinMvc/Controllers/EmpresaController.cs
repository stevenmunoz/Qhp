﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using jardinMvc.Filters;
using jardinMvc.Models;
using PagedList;
using CodeCarvings.Piczard;
using System.Drawing;


namespace jardinMvc.Controllers
{

    [Authorize]
    [InitializeSimpleMembership]
    public class EmpresaController : Controller
    {
        private UnifyEntities db = new UnifyEntities();
        
        //
        // GET: /Empresa/

        public ActionResult Index()
        {
            var tbl_empresa = db.tbl_empresa.Include(t => t.tbl_categoria);
            return View(tbl_empresa.ToList());
        }

        public int CountEmpresas()
        {
            UnifyEntities db = new UnifyEntities();

            return db.tbl_empresa.Count();
        }

        //
        // GET: /Empresa/Details/5

        public ActionResult Details(int id = 0)
        {
            tbl_empresa tbl_empresa = db.tbl_empresa.Find(id);
            if (tbl_empresa == null)
            {
                return HttpNotFound();
            }
            return View(tbl_empresa);
        }

        //
        // GET: /Empresa/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Empresa/Create

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(RegisterModel model, string nombre)
        {
            tbl_empresa empresa = new tbl_empresa();

            if (ModelState.IsValid)
            {
                // Intento de registrar al usuario

                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    System.Web.Security.Roles.AddUserToRole(model.UserName, "Empresa");

                    empresa.empresa_nombre = nombre;
                    empresa.empresa_nombre_url = ProcesarCadenasModel.URLFriendly(ProcesarCadenasModel.RemoveDiacritics(empresa.empresa_nombre));
                    empresa.empresa_descripcion = "Descripción";
                    empresa.empresa_correo = "ejemplo@correo.com";
                    empresa.empresa_estado = true;
                    empresa.userName = model.UserName;

                    db.tbl_empresa.Add(empresa);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            RegisterModel dataModel = new RegisterModel();
            dataModel.UserName = model.UserName;
            dataModel.Password = model.Password;
            dataModel.ConfirmPassword = model.ConfirmPassword;
            return View(dataModel);
        }

        //
        // GET: /Empresa/Edit/5
        public ActionResult Edit(int id = 0)
        {
            tbl_empresa tbl_empresa = db.tbl_empresa.Find(id);
            if (tbl_empresa == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_empresa.categoria_id);

            tbl_empresa.tbl_imagen = db.tbl_imagen.Where(m => m.empresa_id == tbl_empresa.empresa_id).ToList();

            return View(tbl_empresa);
        }

        //
        // POST: /Empresa/Edit/5

        [HttpPost]
        public ActionResult Edit(tbl_empresa tbl_empresa, HttpPostedFileBase empresa_imagen, IEnumerable<HttpPostedFileBase> images, string[] checkSubcategorias, string imagenAnt)
        {
            if (ModelState.IsValid)
            {
                UnifyEntities entities = new UnifyEntities();

                tbl_empresa emp = entities.tbl_empresa.Find(tbl_empresa.empresa_id);
                foreach(var item in emp.tbl_subcategoria.ToList())
                {
                    emp.tbl_subcategoria.Remove(item);
                }
                if(checkSubcategorias != null)
                {
                    foreach(var item in checkSubcategorias)
                    {
                        emp.tbl_subcategoria.Add(entities.tbl_subcategoria.Find(int.Parse(item)));
                    }
                }
                entities.SaveChanges();

                if (empresa_imagen != null)
                {
                    string ruta = Server.MapPath("~/Images/Empresa/");
                    string[] nomImagenVec = empresa_imagen.FileName.Split('.');
                    string extension = nomImagenVec[1];
                    string nombreSistema = GetTimestamp(DateTime.Now);
                    string rutaRelativa = ruta + nombreSistema + "." + extension;
                    string nombreImagen = nombreSistema + "." + extension;

                    //Variable que indica si existe una imagen con el mismo nombre en base de datos
                    int cantImagenes = db.tbl_empresa.Where(m => m.empresa_imagen == nombreSistema).Count();

                    if (cantImagenes == 0)
                    {
                        if (empresa_imagen.ContentLength < (5000 * 1024))
                        {
                            string ruta1 = Server.MapPath("~/Images/Empresa/Thumbs/");
                            string rutaThumb = ruta1 + nombreSistema + "." + extension;

                            tbl_empresa.empresa_imagen = nombreImagen;

                            eliminarArchivoFisico(ruta, emp.empresa_imagen);
                            eliminarArchivoFisicoThumbs(ruta1, emp.empresa_imagen);

                            empresa_imagen.SaveAs(rutaRelativa);

                            RedimensionarImagen(nombreImagen);
                        }
                        else 
                        {
                            ModelState.AddModelError("", "La imagen debe tener un peso máximo de 5mb");

                            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_empresa.categoria_id);
                            tbl_empresa.tbl_imagen = db.tbl_imagen.Where(m => m.empresa_id == tbl_empresa.empresa_id).ToList();
                            tbl_empresa.empresa_imagen = imagenAnt;
                            return View(tbl_empresa);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "El nombre de la imagen ya se encuentra registrado");
                        
                        ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_empresa.categoria_id);
                        tbl_empresa.tbl_imagen = db.tbl_imagen.Where(m => m.empresa_id == tbl_empresa.empresa_id).ToList();
                        tbl_empresa.empresa_imagen = imagenAnt;
                        return View(tbl_empresa);
                    }     
                }
                else 
                {
                    tbl_empresa.empresa_imagen = imagenAnt;
                }

                tbl_empresa.empresa_nombre_url = ProcesarCadenasModel.URLFriendly(ProcesarCadenasModel.RemoveDiacritics(tbl_empresa.empresa_nombre));
                db.Entry(tbl_empresa).State = EntityState.Modified;
                db.SaveChanges();

                if (images != null)
                {
                    saveImages(images, tbl_empresa.empresa_id);
                }

                TempData["Msg"] = "Datos Actualizados Correctamente";
                ModelState.Clear();

                return RedirectToAction("Edit", "Empresa", new { id = tbl_empresa.empresa_id });
            }
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", tbl_empresa.categoria_id);
            return View(tbl_empresa);
        }

        //Guarda la galería de imagenes
        public void saveImages(IEnumerable<HttpPostedFileBase> files, int empCodigo)
        {
            string rutaGuardar = Server.MapPath("~/Images/Empresa/Galeria/");
            string ruta1 = Server.MapPath("~/Images/Empresa/GaleriaThumbs/");
            

            for (int i = 0; i < files.Count(); i++)
            {
                if (files.ElementAt(i) != null)
                {
                    //si las imágenes son menores a 5 mb se almacenan
                    if (files.ElementAt(i).ContentLength < (5000 * 1024))
                    {
                        string[] nomImagenVec = files.ElementAt(i).FileName.Split('.');
                        string extension = nomImagenVec[1];
                        string nombreSistema = GetTimestamp(DateTime.Now);

                        string rutaRelativa = rutaGuardar + nombreSistema + "." + extension;
                        string nombreImagen = nombreSistema + "." + extension;

                        string rutaThumb = ruta1 + nombreSistema + "." + extension;

                        files.ElementAt(i).SaveAs(rutaRelativa);

                        RedimensionarImagenGalería(nombreImagen);

                        //ImageResizer resizer = new ImageResizer(rutaRelativa);
                        //var byteArray = resizer.Resize(90, 90, true, ImageEncoding.Jpg);
                        //resizer.SaveToFile(rutaThumb);

                        tbl_imagen img = new tbl_imagen();
                        img.imagen_id = 1;
                        img.imagen_nombre = nombreImagen;
                        img.empresa_id = empCodigo;
                        db.tbl_imagen.Add(img);
                        db.SaveChanges();
                    }
                }

            }

        }

        public String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        //Elimina una imagen
        public ActionResult EliminarImagen(int imgCodigo)
        {
            int codEmpresa = 0;

            UnifyEntities entities = new UnifyEntities();

            tbl_imagen tbl_imagen = entities.tbl_imagen.Find(imgCodigo);

            if(tbl_imagen != null)
            {
                codEmpresa = int.Parse(tbl_imagen.empresa_id.ToString());

                string ruta = Server.MapPath("~/Images/Empresa/Galeria/");
                string rutaThumb = Server.MapPath("~/Images/Empresa/GaleriaThumbs/");

                eliminarArchivoFisico(ruta, tbl_imagen.imagen_nombre);
                eliminarArchivoFisicoThumbs(rutaThumb, tbl_imagen.imagen_nombre);

                entities.tbl_imagen.Remove(tbl_imagen);
                entities.SaveChanges();

                return RedirectToAction("Edit", "Empresa", new { id = codEmpresa });
            }
            return RedirectToAction("Index", "Empresa");
        }

        public void eliminarArchivoFisico(string ruta, string docRuta)
        {
            string rutaRelativa = ruta + docRuta;
            //Existe el archivo ?
            if (System.IO.File.Exists(rutaRelativa))
            {
                System.IO.File.Delete(rutaRelativa);
            }
        }

        public void eliminarArchivoFisicoThumbs(string ruta, string docRuta)
        {
            string rutaRelativa = ruta + docRuta;
            //Existe el archivo ?
            if (System.IO.File.Exists(rutaRelativa))
            {
                System.IO.File.Delete(rutaRelativa);
            }
        }

        //
        // GET: /Empresa/Delete/5

        public ActionResult Delete(int id = 0)
        {
            tbl_empresa tbl_empresa = db.tbl_empresa.Find(id);
            if (tbl_empresa == null)
            {
                return HttpNotFound();
            }
            return View(tbl_empresa);
        }

        //
        // POST: /Empresa/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            tbl_empresa tbl_empresa = db.tbl_empresa.Find(id);
            db.tbl_empresa.Remove(tbl_empresa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Vaya a http://go.microsoft.com/fwlink/?LinkID=177550 para
            // obtener una lista completa de códigos de estado.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "El nombre de usuario ya existe. Escriba un nombre de usuario diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Ya existe un nombre de usuario para esa dirección de correo electrónico. Escriba una dirección de correo electrónico diferente.";

                case MembershipCreateStatus.InvalidPassword:
                    return "La contraseña especificada no es válida. Escriba un valor de contraseña válido.";

                case MembershipCreateStatus.InvalidEmail:
                    return "La dirección de correo electrónico especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "La respuesta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "La pregunta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidUserName:
                    return "El nombre de usuario especificado no es válido. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.ProviderError:
                    return "El proveedor de autenticación devolvió un error. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                case MembershipCreateStatus.UserRejected:
                    return "La solicitud de creación de usuario se ha cancelado. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                default:
                    return "Error desconocido. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";
            }
        }

        public ActionResult estadoEmpresa(int empresa_id)
        {
            UnifyEntities entities = new UnifyEntities();
            tbl_empresa empresa = entities.tbl_empresa.Find(empresa_id);
            if(empresa != null)
            {
                if (empresa.empresa_estado == true)
                {
                    empresa.empresa_estado = false;
                }
                else 
                {
                    empresa.empresa_estado = true;
                }
            }
            entities.SaveChanges();
            return Json(new { nEstado = empresa.empresa_estado.ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubcategoriasEmpresa(int categoria_id, int empresa_id)
        {
            UnifyEntities entities = new UnifyEntities();

            tbl_empresa empresa = new tbl_empresa();
            List<tbl_subcategoria> subcatEmpresas = new List<tbl_subcategoria>();
            var empresaSel = entities.tbl_empresa.Find(empresa_id);
            if(empresaSel != null)
            {
                foreach(var item in empresaSel.tbl_subcategoria)
                {
                    subcatEmpresas.Add(new tbl_subcategoria { subcategoria_id = item.subcategoria_id, subcategoria_nombre = item.subcategoria_nombre, categoria_id = item.categoria_id });
                }
            }
            empresa.tbl_subcategoria = subcatEmpresas;

            var resultado = from lista in entities.tbl_subcategoria
                            where lista.categoria_id == categoria_id
                            select new Models.SubcategoriaModel { check = "",  identificador = lista.subcategoria_id, nombre = lista.subcategoria_nombre };
            
            List<string> listId = new List<string>();
            foreach(var cat in subcatEmpresas)
            {
                listId.Add(cat.subcategoria_id.ToString());
            }
            List<Models.SubcategoriaModel> subCats = new List<SubcategoriaModel>();
            foreach(var res in resultado)
            {
                if(listId.Contains(res.identificador.ToString()))
                {
                    res.check = "checked";
                    subCats.Add(res);
                }
                else
                {
                    subCats.Add(res);
                }
            }

            return Json(subCats, JsonRequestBehavior.AllowGet);
        }

        //Redimensiona imágenes
        public void RedimensionarImagen(string NombreImagen)
        {
            //Thumb Empresa
            new FixedCropConstraint(175, 120).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/" + NombreImagen), Server.MapPath("~/Images/Empresa/Thumbs/" + NombreImagen));

            //Principal Empresa
            new FixedCropConstraint(400, 245).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/" + NombreImagen), Server.MapPath("~/Images/Empresa/" + NombreImagen));
        }

        //Redimensiona imágenes
        public void RedimensionarImagenGalería(string NombreImagen)
        {
            //Thumb Galeria
            new FixedCropConstraint(90, 90).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen), Server.MapPath("~/Images/Empresa/GaleriaThumbs/" + NombreImagen));

            //Principal Galeria
            new FixedCropConstraint(452, 302).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen), Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen));
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}