﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jardinMvc.Context;
using jardinMvc.Models;

namespace jardinMvc
{
    public class ListadoEventosRepository : IEventos
    {
        protected UnifyEntities DataContext { get; private set; }

        public ICacheProvider Cache { get; set; }

        public ListadoEventosRepository()
            : this(new DefaultCacheProvider())
        {
        }

        public ListadoEventosRepository(ICacheProvider cacheProvider)
        {
            this.DataContext = new UnifyEntities();
            this.Cache = cacheProvider;
        }

        public IEnumerable<tbl_evento_empresa> GetListadoEventos()
        {
            // First, check the cache
            IEnumerable eventosData = Cache.Get("listadoEventos") as IEnumerable;
            
            // If it's not in the cache, we need to read it from the repository
            if (eventosData == null)
            {
                // Get the repository data
                var eventos = DataContext.tbl_evento_empresa.Where(m => m.evento_fechaInicio <= DateTime.Now && DateTime.Now < m.evento_fechaFin && m.tbl_empresa.empresa_estado).OrderByDescending(m => m.evento_fechaInicio);
                eventosData = eventos;

                if (eventosData != null)
                {
                    // Put this data into the cache for 120 minutes
                    Cache.Set("listadoEventos", eventosData, 120);
                }
            }

            return eventosData as IEnumerable<tbl_evento_empresa>;
        }

        public void ClearCache()
        {
            Cache.Invalidate("listadoEventos");
        }

    }

}