﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jardinMvc.Models;
using jardinMvc.Context;

namespace jardinMvc
{
    //Pretty simply, it defines contract which allows us to get the field. It also allows us to clear it’s data cache
    public interface IEventos
    {
        void ClearCache();
        IEnumerable<tbl_evento_empresa> GetListadoEventos();
    }
}
