﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;

namespace jardinMvc
{
    public class DefaultCacheProvider : ICacheProvider
    {
        //private property which allows us to retrieve the actual cache object. 
        //This just returns the static .Net4 MemoryCache object. Because it’s static,
        //you will receive the same cache object back every time you call it, 
        //and other users using the cache will also be served the same cache object. 
        //This is key – once the data is in the cache, it is cached for all users.
        private ObjectCache Cache { get { return MemoryCache.Default; } }

        public object Get(string key)
        {
            return Cache[key];
        }

        //In our simple implementation of the Set method, we create a cache policy which caches the item for the following 30 minutes. 
        //Once the 30 minutes has passed, the item is dropped from the cache.
        public void Set(string key, object data, int cacheTime)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(cacheTime);

            Cache.Add(new CacheItem(key, data), policy);
        }

        //We have another couple of utility methods which allow consumers to check that an item has been cached, 
        //and also one which allows us to force the removal of a cached object.
        public bool IsSet(string key)
        {
            return (Cache[key] != null);
        }

        public void Invalidate(string key)
        {
            Cache.Remove(key);
        }
    }
}