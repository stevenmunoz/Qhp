﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jardinMvc.Models
{
    public class SubcategoriaModel
    {
        public int identificador { get; set; }
        public string nombre { get; set; }
        public string check { get; set; }
    }
}