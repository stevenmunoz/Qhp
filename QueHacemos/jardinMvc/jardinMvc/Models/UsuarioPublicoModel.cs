﻿using jardinMvc.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace jardinMvc.Models
{
    public class UsuarioPublicoModel
    {
        
        public UsuarioPublicoModel()
        {
            this.tbl_comentario_empresa = new HashSet<tbl_comentario_empresa>();
            this.tbl_empresa_calificacion = new HashSet<tbl_empresa_calificacion>();
            this.tbl_evento_empresa = new HashSet<tbl_evento_empresa>();
            this.tbl_imagen = new HashSet<tbl_imagen>();
            this.tbl_subcategoria = new HashSet<tbl_subcategoria>();
        }

        [Display(Name = "Id")]
        public int empresa_ide { get; set; }

        [Required(ErrorMessage = "El campo nombre de empresa es requerido")]
        [Display(Name = "Nombre Empresa")]
        public string empresa_nombre { get; set; }

        [Required(ErrorMessage = "El campo nombre de usuario es requerido")]
        [Display(Name = "Nombre de usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "El campo contraseña es requerido")]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Quiénes Somos")]
        [Required(ErrorMessage = "El campo quiénes somos es requerido")]
        public string empresa_descripcion { get; set; }

        [Display(Name = "Teléfono Público")]
        [Required(ErrorMessage = "El campo teléfono público es requerido")]
        public Nullable<long> empresa_telefono { get; set; }

        [Display(Name = "Imagen de Perfil")]
        [Required(ErrorMessage = "El campo imagen de perfil es requerido")]
        public string empresa_imagen { get; set; }

        [Display(Name = "Estado")]
        public bool empresa_estado { get; set; }

        [Display(Name = "Correo")]
        [EmailAddress(ErrorMessage = "Dirección de correo inválida")]
        [Required(ErrorMessage = "El campo correo es requerido")]
        public string empresa_correo { get; set; }

        [Display(Name = "Categoría")]
        public Nullable<int> categoria_id { get; set; }
    
        [Display(Name = "Facebook")]
        [RegularExpression(@"^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$", ErrorMessage = "URL Inválida")]
        public string empresa_facebook { get; set; }

        [Display(Name = "Twitter")]
        [RegularExpression(@"^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$", ErrorMessage = "URL Inválida")]
        public string empresa_twitter { get; set; }

        [Display(Name = "Teléfono de Contacto")]
        public Nullable<long> empresa_telContacto { get; set; }

        public string empresa_nombre_url { get; set; }

        public int empresa_calific_total { get; set; }

        public virtual tbl_categoria tbl_categoria { get; set; }
        public virtual ICollection<tbl_comentario_empresa> tbl_comentario_empresa { get; set; }
        public virtual ICollection<tbl_empresa_calificacion> tbl_empresa_calificacion { get; set; }
        public virtual ICollection<tbl_evento_empresa> tbl_evento_empresa { get; set; }
        public virtual ICollection<tbl_imagen> tbl_imagen { get; set; }
        public virtual ICollection<tbl_subcategoria> tbl_subcategoria { get; set; }
    }
}