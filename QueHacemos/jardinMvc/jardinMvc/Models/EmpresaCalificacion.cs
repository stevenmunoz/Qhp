﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jardinMvc.Models
{
    public class EmpresaCalificacion
    {
        public int empresa_id { get; set; }
        public string empresa_nombre { get; set; }
        public string empresa_descripcion { get; set; }
        public string empresa_imagen { get; set; }
        public bool empresa_estado { get; set; }
        public string empresa_correo { get; set; }
        public Nullable<int> categoria_id { get; set; }
        public string userName { get; set; }
        public Nullable<Int64> empresa_telefono { get; set; }
        public int empresa_calificacion { get; set; }
        public string empresa_nombre_url { get; set; }
    }
}