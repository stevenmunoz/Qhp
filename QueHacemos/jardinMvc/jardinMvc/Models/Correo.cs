﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace jardinMvc.Models
{
    public class Correo
    {
        public static bool SendMail(string gMailAccount, string password, string to, string subject, string message, string bcc, string DisplayName, string pAttachmentPath)
        {
            try
            {
                //SmtpClient client = new SmtpClient();
                //client.Port = 25;
                //client.Host = "mail.quehacemospues.com";
                ////client.EnableSsl = true;
                //client.Timeout = 10000;
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.UseDefaultCredentials = false;
                //client.Credentials = new System.Net.NetworkCredential("steven@quehacemospues.com", "Monito1990");

                //MailMessage mm = new MailMessage("steven@quehacemospues.com", "steven.munoz90@gmail.com", "test", "test");
                //mm.BodyEncoding = UTF8Encoding.UTF8;
                //mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                //client.Send(mm);


                NetworkCredential loginInfo = new NetworkCredential(gMailAccount, password);
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(gMailAccount, DisplayName);
                msg.To.Add(new MailAddress(to));
                if (bcc != "")
                    msg.Bcc.Add(new MailAddress(bcc));
                msg.Subject = subject;
                msg.Body = message;
                msg.IsBodyHtml = true;

                if (pAttachmentPath.Trim() != "0")
                {
                    msg.Attachments.Add(new Attachment(pAttachmentPath));
                }

                msg.Priority = MailPriority.High;
                SmtpClient client = new SmtpClient("mail.quehacemospues.com", 25);
                //client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = loginInfo;
                client.Send(msg);

                return true;
            }
            catch (Exception e)
            {
                e.ToString();
                return false;
            }
        }
    }
}