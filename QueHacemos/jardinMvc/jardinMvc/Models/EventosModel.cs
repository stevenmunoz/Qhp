﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jardinMvc.Models
{
    public class EventosModel
    {
        public string nombre_evento { get; set; }
        public string descripcion_evento { get; set; }
        public System.DateTime fecha_evento { get; set; }
        public string imagen_empresa { get; set; }
        public int idEmpresa { get; set; }
        public string nombreEmpresa { get; set; }
    }
}