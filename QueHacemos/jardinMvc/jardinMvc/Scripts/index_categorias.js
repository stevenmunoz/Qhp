function limpiar() {
    var validator = $("#form-contacto").validate();
    validator.resetForm();
    $("#name").val("");
    $("#email").val("");
    $("#message").val("");
    $("#enviando").css("display", "none");
}

function enviar()
{
    var name = $("#name").val();
    var email = $("#email").val();
    var message = $("#message").val();
    if(name != "" && email != "" && message != "")
    {
        $("#enviando").css("display", "block");
    }
}

$(document).ready(function () {
    $('#form-contacto').validate({
        rules: {
            name: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                minlength: 2,
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        }
    });

    function parseJsonDate(jsonDateString) {
        return new Date(parseInt(jsonDateString.replace('/Date(', '')));
    }

    function getFormattedDate(date) {
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = date.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return day + '/' + month + '/' + year;
    }

    $.getJSON("/Home/GetListadoEventos/", {}, function (result) {
        var template = $('#listado-eventos-template').clone().html();
        var html = "";
        $.each(result, function (i, field) {
            var date = parseJsonDate(field.eventoFecha);
            date = getFormattedDate(date);
            html += template.replace('{{eventoDescripcion}}', field.eventoDescripcion)
                            .replace('{{eventoNombre}}', field.eventoNombre)
                            .replace('{{empresaImagen}}', field.empresaImagen)
                            .replace('{{empresaId}}', field.empresaId)
                            .replace('{{empresaNombreUrl}}', field.empresaNombreUrl)
                            .replace('{{eventoFecha}}', date)
        });

        $('.ulEstilo').html(html);
    });
}); // end document.ready

