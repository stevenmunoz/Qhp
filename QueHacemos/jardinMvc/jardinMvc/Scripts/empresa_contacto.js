function limpiarContact() {
    var validator = $("#form-contacto").validate();
    validator.resetForm();
    $("#nomContact").val("");
    $("#corContact").val("");
    $("#mensContact").val("");
    $("#enviaContact").css("display", "none");
}

function enviarContact()
{
    var name = $("#nomContact").val();
    var email = $("#corContact").val();
    var message = $("#mensContact").val();
    if(name != "" && email != "" && message != "")
    {
        $("#enviaContact").css("display", "block");
    }
}

function limpiarComent() {
    var validator = $("#form-coment").validate();
    validator.resetForm();
    $("#nomComent").val("");
    $("#corComent").val("");
    $("#mensComent").val("");
    $("#enviaComent").css("display", "none");
}

function enviarComent() {
    var nom = $("#nomComent").val();
    var cor = $("#corComent").val();
    var coment = $("#mensComent").val();
    if (nom != "" && cor != "" && coment != "") {
        $("#enviaComent").css("display", "block");
    }
}

$(document).ready(function () {
    $('#form-contacto').validate({
        rules: {
            nomContact: {
                minlength: 2,
                required: true
            },
            corContact: {
                required: true,
                email: true
            },
            mensContact: {
                minlength: 2,
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        }
    });

    $('#form-coment').validate({
        rules: {
            nomComent: {
                minlength: 2,
                required: true
            },
            corComent: {
                required: true,
                email: true
            },
            mensComent: {
                minlength: 2,
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        }
    });
}); // end document.ready

