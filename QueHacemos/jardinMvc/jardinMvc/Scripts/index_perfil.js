$(document).ready(function () {
    $('#form-coment').validate({
        rules: {
            name: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email: true
            },
            comment: {
                minlength: 2,
                required: true
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        }        
    });
}); // end document.ready

function enviarComent() {
    var name = $("#name").val();
    var email = $("#email").val();
    var coment = $("#comment").val();
    if (name != "" && email != "" && coment != "") {
        $("#enviando").css("display", "block");
    }
}
