$(function(){
	
        var currentdate = new Date();
        //date_future = new Date(2014, 01, 10, 00, 00, 00, 0);
        date_future = new Date(2014, 01, 23, 00, 00, 00, 0);
        date_now = new Date();
        
        if (currentdate > date_future)
        {
            document.location = '/Home/';
        }
        
        seconds = Math.floor((date_future - (date_now))/1000);
        minutes = Math.floor(seconds/60);
        hours = Math.floor(minutes/60);
        days = Math.floor(hours/24);
        
        hours = hours-(days*24);
        minutes = minutes-(days*24*60)-(hours*60);
        seconds = seconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
        
        $('#counter').countdown({
          image: '../Images/digits.png',
          startTime: days+':'+hours+':'+minutes+':'+seconds,
          timerEnd: function () {
              document.location = '/Home/';
          },
          format: 'dd:hh:mm:ss'
        });
});
