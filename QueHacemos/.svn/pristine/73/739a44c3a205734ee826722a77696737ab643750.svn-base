﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jardinMvc.Context;
using System.Collections;
using jardinMvc.Models;
using WebMatrix.WebData;
using System.Web.Security;
using CodeCarvings.Piczard;
using System.Data;
using jardinMvc.Filters;

namespace jardinMvc.Controllers
{
    [InitializeSimpleMembership]
    public class PerfilController : Controller
    {
        private UnifyEntities db = new UnifyEntities();
        //
        // GET: /Perfil/

        public ActionResult Empresa(int id)
        {
            tbl_empresa empresa = db.tbl_empresa.Find(id);
            ViewBag.cantidadCalificaciones = cantidadCalificaciones(id);
            return View(empresa);
        }

        public ActionResult Comentario(string nomComent, string corComent, string mensComent, int empresa_id)
        {
            if (nomComent != null && !String.IsNullOrWhiteSpace(mensComent))
            {
                tbl_comentario_empresa comentario = new tbl_comentario_empresa();
                comentario.comentario_nombre = nomComent;
                comentario.comentario_correo = corComent;
                comentario.comentario_mensaje = mensComent;
                comentario.comentario_fecha = DateTime.Now;
                comentario.empresa_id = empresa_id;
                db.tbl_comentario_empresa.Add(comentario);
                try
                {
                    db.SaveChanges();
                    TempData["comen"] = 1;
                }
                catch (Exception ex)
                {
                    TempData["comen"] = 0;
                }
            }
            return RedirectToAction("Empresa", "Perfil", new { id = empresa_id });
        }

        public ActionResult calificarEmpresa(int empresa_id, int calificacion)
        {
            //Agregamos la calificación de la empresa
            tbl_empresa_calificacion califica = new tbl_empresa_calificacion();
            califica.empresa_id = empresa_id;
            califica.tipCalifica_id = calificacion;
            db.tbl_empresa_calificacion.Add(califica);
            try
            {
                db.SaveChanges();
                //Actualizamos el total de calificación
                tbl_empresa empresa = db.tbl_empresa.Find(empresa_id);
                empresa.empresa_calific_total = Calificacion(empresa_id);
                db.SaveChanges();
                return Json(new { estado = "True" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string msg = ex.ToString();
                return Json(new { estado = "False" }, JsonRequestBehavior.AllowGet);
            }
        }

        public int Calificacion(int empresa_id)
        {
            float total = 0;
            int promedio = 0;

            List<tbl_empresa_calificacion> calificacion = new List<tbl_empresa_calificacion>();
            var listado = db.tbl_empresa_calificacion.Where(m => m.empresa_id == empresa_id).ToList();
            if (listado.Count > 0)
            {
                var suma = listado.Sum(m => m.tipCalifica_id);
                total = float.Parse(suma.ToString()) / float.Parse(listado.Count().ToString());
                promedio = int.Parse(Math.Round(total).ToString());
            }
            else
            {
                promedio = 0;
            }
            return promedio;
        }

        public List<tbl_comentario_empresa> ListaComentarios(int empresa_id)
        {
            List<tbl_comentario_empresa> comentarios = new List<tbl_comentario_empresa>();
            tbl_empresa empresa = db.tbl_empresa.Find(empresa_id);

            comentarios = empresa.tbl_comentario_empresa.ToList();

            return comentarios;
        }

        public int cantidadCalificaciones(int idEmpresa)
        {
            return db.tbl_empresa_calificacion.Where(x => x.empresa_id == idEmpresa).Count();
        }

        public ActionResult Crear()
        {

            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", 1);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Crear(UsuarioPublicoModel model, string[] checkSubcategorias, HttpPostedFileBase empresa_imagen, IEnumerable<HttpPostedFileBase> images)
        {
            tbl_empresa empresa = new tbl_empresa();

            if (ModelState.IsValid)
            {
                // Intento de registrar al usuario

                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password);
                    System.Web.Security.Roles.AddUserToRole(model.UserName, "Empresa");

                    empresa.empresa_nombre = model.empresa_nombre;
                    empresa.empresa_nombre_url = ProcesarCadenasModel.URLFriendly(ProcesarCadenasModel.RemoveDiacritics(empresa.empresa_nombre));
                    empresa.empresa_descripcion = model.empresa_descripcion;
                    empresa.empresa_correo = model.empresa_correo;
                    empresa.empresa_estado = true;
                    empresa.userName = model.UserName;
                    empresa.empresa_telefono = model.empresa_telefono;
                    empresa.categoria_id = model.categoria_id;
                    empresa.empresa_telContacto = model.empresa_telContacto != null ? model.empresa_telContacto : null;
                    empresa.empresa_facebook = String.IsNullOrEmpty(model.empresa_facebook) ? string.Empty : model.empresa_facebook;
                    empresa.empresa_twitter = String.IsNullOrEmpty(model.empresa_twitter) ? string.Empty : model.empresa_twitter;
                    

                    db.tbl_empresa.Add(empresa);
                    db.SaveChanges();

                    tbl_empresa empresaModif = db.tbl_empresa.Find(empresa.empresa_id);

                    if (checkSubcategorias != null)
                    {
                        foreach (var item in checkSubcategorias)
                        {
                            empresaModif.tbl_subcategoria.Add(db.tbl_subcategoria.Find(int.Parse(item)));
                        }
                    }

                    //Registro de imagen de perfil
                    if (empresa_imagen != null)
                    {
                        string ruta = Server.MapPath("~/Images/Empresa/");
                        string[] nomImagenVec = empresa_imagen.FileName.Split('.');
                        string extension = nomImagenVec[1];
                        string nombreSistema = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                        string rutaRelativa = ruta + nombreSistema + "." + extension;
                        string nombreImagen = nombreSistema + "." + extension;

                        if (empresa_imagen.ContentLength < (5000 * 1024))
                        {
                            string ruta1 = Server.MapPath("~/Images/Empresa/Thumbs/");
                            string rutaThumb = ruta1 + nombreSistema + "." + extension;

                            empresaModif.empresa_imagen = nombreImagen;

                            empresa_imagen.SaveAs(rutaRelativa);
                            RedimensionarImagen(nombreImagen);
                        }
                        else
                        {
                            ModelState.AddModelError("", "La imagen debe tener un peso máximo de 5mb");

                            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", empresa.categoria_id);
                            empresaModif.tbl_imagen = db.tbl_imagen.Where(m => m.empresa_id == empresa.empresa_id).ToList();
                            //empresa.empresa_imagen = imagenAnt;
                            return View(empresa);
                        }
                    }

                    db.SaveChanges();

                    if (images != null)
                    {
                        saveImages(images, empresa.empresa_id);
                    }

                    //Redirección página de categorías basada en los datos del usuario
                    int? idCat = model.categoria_id;
                    string nombreCat = db.tbl_categoria.Where( m => m.categoria_id == model.categoria_id).Select(m => m.categoria_nombre).FirstOrDefault();
                    ModelState.Clear();

                    return RedirectToAction("QueHacemosPues", "Categoria", new { id = idCat, nombre = nombreCat });
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }

            UsuarioPublicoModel dataModel = new UsuarioPublicoModel();
            dataModel.empresa_nombre = model.empresa_nombre;
            dataModel.empresa_nombre_url = ProcesarCadenasModel.URLFriendly(ProcesarCadenasModel.RemoveDiacritics(empresa.empresa_nombre));
            dataModel.empresa_descripcion = model.empresa_descripcion;
            dataModel.empresa_correo = model.empresa_correo;
            dataModel.empresa_estado = true;
            dataModel.UserName = model.UserName;
            dataModel.empresa_telefono = model.empresa_telefono;
            dataModel.categoria_id = model.categoria_id;
            dataModel.empresa_telContacto = model.empresa_telContacto != null ? model.empresa_telContacto : null;
            dataModel.empresa_facebook = String.IsNullOrEmpty(model.empresa_facebook) ? string.Empty : model.empresa_facebook;
            dataModel.empresa_twitter = String.IsNullOrEmpty(model.empresa_twitter) ? string.Empty : model.empresa_twitter;
            ViewBag.categoria_id = new SelectList(db.tbl_categoria, "categoria_id", "categoria_nombre", empresa.categoria_id);
            dataModel.tbl_imagen = db.tbl_imagen.Where(m => m.empresa_id == empresa.empresa_id).ToList();
                            
            return View(dataModel);
        }



        public ActionResult Subcategorias(int categoria_id)
        {
            UnifyEntities entities = new UnifyEntities();

            var resultado = from lista in entities.tbl_subcategoria
                            where lista.categoria_id == categoria_id
                            select new Models.SubcategoriaModel { check = "", identificador = lista.subcategoria_id, nombre = lista.subcategoria_nombre };

            List<Models.SubcategoriaModel> subCats = new List<SubcategoriaModel>();
            foreach (var res in resultado)
            {
                subCats.Add(res);
            }

            return Json(subCats, JsonRequestBehavior.AllowGet);
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Vaya a http://go.microsoft.com/fwlink/?LinkID=177550 para
            // obtener una lista completa de códigos de estado.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "El nombre de usuario ya existe. Escriba un nombre de usuario diferente.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Ya existe un nombre de usuario para esa dirección de correo electrónico. Escriba una dirección de correo electrónico diferente.";

                case MembershipCreateStatus.InvalidPassword:
                    return "La contraseña especificada no es válida. Escriba un valor de contraseña válido.";

                case MembershipCreateStatus.InvalidEmail:
                    return "La dirección de correo electrónico especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "La respuesta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "La pregunta de recuperación de la contraseña especificada no es válida. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.InvalidUserName:
                    return "El nombre de usuario especificado no es válido. Compruebe el valor e inténtelo de nuevo.";

                case MembershipCreateStatus.ProviderError:
                    return "El proveedor de autenticación devolvió un error. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                case MembershipCreateStatus.UserRejected:
                    return "La solicitud de creación de usuario se ha cancelado. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";

                default:
                    return "Error desconocido. Compruebe los datos especificados e inténtelo de nuevo. Si el problema continúa, póngase en contacto con el administrador del sistema.";
            }
        }

        public void eliminarArchivoFisico(string ruta, string docRuta)
        {
            string rutaRelativa = ruta + docRuta;
            //Existe el archivo ?
            if (System.IO.File.Exists(rutaRelativa))
            {
                System.IO.File.Delete(rutaRelativa);
            }
        }

        public void eliminarArchivoFisicoThumbs(string ruta, string docRuta)
        {
            string rutaRelativa = ruta + docRuta;
            //Existe el archivo ?
            if (System.IO.File.Exists(rutaRelativa))
            {
                System.IO.File.Delete(rutaRelativa);
            }
        }
        
        //Redimensiona imágenes
        public void RedimensionarImagen(string NombreImagen)
        {
            //Thumb Empresa
            new FixedCropConstraint(175, 120).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/" + NombreImagen), Server.MapPath("~/Images/Empresa/Thumbs/" + NombreImagen));

            //Principal Empresa
            new FixedCropConstraint(400, 245).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/" + NombreImagen), Server.MapPath("~/Images/Empresa/" + NombreImagen));
        }

        //Redimensiona imágenes
        public void RedimensionarImagenGalería(string NombreImagen)
        {
            //Thumb Galeria
            new FixedCropConstraint(90, 90).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen), Server.MapPath("~/Images/Empresa/GaleriaThumbs/" + NombreImagen));

            //Principal Galeria
            new FixedCropConstraint(452, 302).SaveProcessedImageToFileSystem(Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen), Server.MapPath("~/Images/Empresa/Galeria/" + NombreImagen));
        }

        //Guarda la galería de imagenes
        public void saveImages(IEnumerable<HttpPostedFileBase> files, int empCodigo)
        {
            string rutaGuardar = Server.MapPath("~/Images/Empresa/Galeria/");
            string ruta1 = Server.MapPath("~/Images/Empresa/GaleriaThumbs/");


            for (int i = 0; i < files.Count(); i++)
            {
                if (files.ElementAt(i) != null)
                {
                    //si las imágenes son menores a 5 mb se almacenan
                    if (files.ElementAt(i).ContentLength < (5000 * 1024))
                    {
                        string[] nomImagenVec = files.ElementAt(i).FileName.Split('.');
                        string extension = nomImagenVec[1];
                        string nombreSistema = DateTime.Now.ToString("yyyyMMddHHmmssffff");

                        string rutaRelativa = rutaGuardar + nombreSistema + "." + extension;
                        string nombreImagen = nombreSistema + "." + extension;

                        string rutaThumb = ruta1 + nombreSistema + "." + extension;

                        files.ElementAt(i).SaveAs(rutaRelativa);

                        RedimensionarImagenGalería(nombreImagen);

                        //ImageResizer resizer = new ImageResizer(rutaRelativa);
                        //var byteArray = resizer.Resize(90, 90, true, ImageEncoding.Jpg);
                        //resizer.SaveToFile(rutaThumb);

                        tbl_imagen img = new tbl_imagen();
                        img.imagen_id = 1;
                        img.imagen_nombre = nombreImagen;
                        img.empresa_id = empCodigo;
                        db.tbl_imagen.Add(img);
                        db.SaveChanges();
                    }
                }

            }

        }
    }
}
